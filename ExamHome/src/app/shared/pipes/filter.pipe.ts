import { Pipe, PipeTransform } from '@angular/core';
import { isNullOrUndefined } from 'util';
import { list } from '../modules/list/list.module';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
search:"";
  transform(lists, search) {


if (!isNullOrUndefined(lists) && search.trim() !== "") {
  let filter = lists.filter(
    list => list.content.toLowerCase().indexOf(search.toLowerCase()) !== -1);
  return filter;
}
else
return lists;

}

}
