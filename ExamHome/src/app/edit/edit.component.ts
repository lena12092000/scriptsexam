import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ListService } from '../shared/services/list.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  FormEdit:FormGroup;
  disabled = true;
  id:number;
  list;

  constructor(private activeRoute: ActivatedRoute,
    private listService: ListService,
    private router:Router
    ) { 
    this.activeRoute.params.subscribe(param => {
      this.id = param.id;
      
    })
  }

  ngOnInit() {
    this.getList().then(() => {
    this.FormEdit = new FormGroup({
      content: new FormControl({value:this.list.content.value, disabled:this.disabled}, Validators.required),
      dateEnd:new FormControl({value:this.list.dateEnd.value, disabled:this.disabled}, Validators.required)
    });
  })
}

async getList(){
  
  try{
    this.list = await this.listService.getById(this.id)
  }catch(e){
    console.log(e);
  }
}

async edit(content:string, dateEnd:string){
  try{
    await this.listService.putById(
      this.id,
      {
        content:content,
        status:this.list.status,
        dateEnd:dateEnd
      }
      );
    this.router.navigate(['/']);
  }catch(e){
    console.log(e);
  }
}

  public mask = [/[0-9]/,/[0-9]/,'.',/[0-9]/,/[0-9]/,'.',/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/]
}
