import { Pipe, PipeTransform } from '@angular/core';
import { isNullOrUndefined } from 'util';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {
  

  transform(lists,  sort='every' ) {
    if(!isNullOrUndefined(lists) && ((sort).trim()) !== ""){
      if (sort == 'every'){
        return lists;
      }
      if (sort == 'doNot'){
        let list = lists.filter(
          lists => lists.status === false
        );
        return list;
      }
      else if(sort == 'do'){
        let list = lists.filter(
          lists => lists.status === true
        );
        return list;
      }
      else if(sort == 'exit'){
        let now = new Date();
        let timeNow = now.getDate()+(now.getMonth()+1)+now.getFullYear();
        let list = lists.filter(
          lists => ((lists.status == false) && ((Number(String(lists.dateEnd).split('.')[0])+
          Number(String(lists.dateEnd).split('.')[1])+Number(String(lists.dateEnd).split('.')[2]))<timeNow))
        );
        return list;

      }
    }

}
}
