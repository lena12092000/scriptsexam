import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ListService } from '../shared/services/list.service';
import { Router, ActivatedRoute } from '@angular/router';
import { isNullOrUndefined } from 'util';
import { list } from '../shared/modules/list/list.module';


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  FormAdd:FormGroup;
  nowDate = new Date();
  disabled = true;
  

  constructor(
    private listService:ListService,
    private router: Router,
    ) { 
    

  }



  ngOnInit() {
    
    this.FormAdd = new FormGroup({
      content: new FormControl({value:"", disabled:this.disabled}, Validators.required),
      dateEnd:new FormControl({value:"", disabled:this.disabled}, Validators.required)
    });
  }
   
  async add(content:string, dateEnd:string) { 
    
    try {
       await this.listService.postNotes(
        {
          "content":content,
          "status":false,
          "dateEnd":dateEnd

        }
      );
      this.router.navigate(['/']);
    } catch(e){
      console.error(e);
    }
  }

  public mask = [/[0-9]/,/[0-9]/,'.',/[0-9]/,/[0-9]/,'.',/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/]
}
