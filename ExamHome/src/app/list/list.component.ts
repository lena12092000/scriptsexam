import { Component, OnInit } from '@angular/core';
import { ListService } from '../shared/services/list.service';
import { list } from '../shared/modules/list/list.module';
import { Router } from '@angular/router';
import { puts } from 'util';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  lists;
  list;
  st;
  now = new Date();
  sortOn="";
  srch="";

  constructor(private ListService:ListService,
    private router: Router
    
    ) {}

  ngOnInit() {
    this.updateData();  //сразу показывает весь список
  }
  async updateData() {

    try
    {
      this.lists = await this.ListService.getAll();

    } catch (e)
    {
      console.log(e);
    }
  }

  async delete(id:number) {
    try
    {
      await this.ListService.deleteById(id);
      this.updateData()
    
     
    } catch (e)
    {
      console.log(e);
    
    }
  }
  time(date){
    let timeNow = this.now.getDate()+(this.now.getMonth()+1)+this.now.getFullYear();
    let t = String(date).split('.');
    let time = Number(t[0])+Number(t[1])+Number(t[2]);
    if (time<timeNow)return false;
    else return true;
    // console.log(timeNow);
    // console.log(time)
}
async change(status, id){
  try
    {
       this.list = await this.ListService.getById(id);
       if (status==true) status = false;
       else status = true;
       this.ListService.putById(
        id,
        {
          content:this.list.content,
          status:status,
          dateEnd:this.list.dateEnd

    });
    this.updateData()
  }
     catch (e)
    {
      console.log(e);
    }


}


}
  
